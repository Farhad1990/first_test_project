# imports
from dash import Dash, html, dcc, Output, Input, dash_table, callback
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
import plotly.graph_objs as go
import numpy as np
from datetime import datetime as dt

mapbox_access_token = 'pk.eyJ1IjoiaC1yLWdoYW5iYXJpIiwiYSI6ImNsMmZ3YnRzbDBldHozYm56MXBnaWdyY3YifQ.v8KZrsYNGTfg8x67b-sOVA'
#--------------------------------------------------------------------------------------
#Preparing the dataframes
print("reading sector db file. please wait...")
sector_df = pd.read_excel("Sector_DB.xlsx")

print("reading ticket file. please wait...")
ticket_df = pd.read_csv("Clarity_09.03.2022.csv")

ticket_df['REPORTED'] = pd.to_datetime(ticket_df['REPORTED'])

# Keeping only the required data
NE_List = ['BTS', 'ENODEB', 'NODEB', 'WBTS']
ticket_df = ticket_df[ticket_df['EQUP_EQUT_ABBREVIATION'].isin(NE_List)]
ticket_df["Issue_Type"] = "KPI_TT"

Avail_KPI = ['TCH_Availability(Nokia_SEG)', 'Cell_Availability_including_blocked_by_user_state(Nokia_UCell)',
             'Cell_Availability_Rate_Include_Blocking(UCell_Eric)', 'TCH_Availability(HU_Cell)',
             'TCH_Availability(Eric_Cell)', 'Radio_Network_Availability_Ratio(Hu_Cell)',
             'cell_availability_include_manual_blocking(Nokia_LTE_CELL_NO_NULL)',
             'Cell_Availability_Rate_Include_Blocking(Cell_EricLTE_NO_NULL)',
             'Cell_Availability_Rate_include_Blocking(Cell_Hu_NO_NULL)'
             ]

ticket_df.loc[ticket_df['DEGRADED_KPI'].isin(Avail_KPI), 'Issue_Type'] = "Avail_TT"
ticket_df = ticket_df.reset_index(drop=True)

# Extracting Site_ID from EQUP_INDEX column, adding Index column and merging with Lat and Long columns
temp = np.where((ticket_df["EQUP_INDEX"].apply(len) > 10),
                ticket_df['EQUP_INDEX'].str.split('|'), #Use the output of this if True
                ticket_df['EQUP_INDEX'].str.split('_') #Else use this.
                )

dft = pd.DataFrame(temp.tolist(), columns=["A", "Site"]) #create a new dataframe with the columns we need
dft['Site'] = dft['Site'].str.strip()

dft.fillna("XX", inplace=True)

#Extracting Site ID
site_temp = np.where((dft["Site"].apply(len) < 8),
                dft['Site'].str[:6], #Use the output of this if True
                dft['Site'].str[:2] + dft['Site'].str[4:8] #Else use this.
                )

site_temp = pd.DataFrame(site_temp.tolist()) # create a new dataframe with the columns we need
ticket_df["Site"] = site_temp
ticket_df["Cell"] = dft['Site']


#Adding Province Index columns
ticket_df["Index"] = ticket_df["Site"].str[:2]
sector_df["Index"] = sector_df["Site"].str[:2]

site_df = sector_df[['Site', 'LATITUDE', 'LONGITUDE', 'Index']].drop_duplicates(subset=['Site'])

ticket_df = ticket_df.merge(site_df[['Site', 'LATITUDE', 'LONGITUDE']], how='left', on='Site')

# dropping rows with no latitude and longitude
ticket_df.dropna(subset=['LATITUDE'], inplace=True)

#Extracting Sector ID
sector_temp = np.where((dft["Site"].apply(len) < 8),
                dft['Site'].str[:], #Use the output of this if True
                dft['Site'].str[:2] + dft['Site'].str[4:9] #Else use this.
                )

sector_temp = pd.DataFrame(sector_temp.tolist()) # create a new dataframe with the columns we need
ticket_df["Sector"] = sector_temp

# print(ticket_df.shape)

ticket_df = ticket_df.merge(sector_df[['Sector', 'AZIMUTH']], how='left', on='Sector')
# what about sectors without azimuth???

#ticket_df.to_csv('test_output.csv', index=False)

avail_df = ticket_df[ticket_df["Issue_Type"]=="Avail_TT"]

ticket_df = ticket_df[ticket_df["Issue_Type"]=="KPI_TT"]
#--------------------------------------------------------------------------------------
# BOOTSTRAP+, CERULEAN+, COSMO+, SKETCHY, SLATE+, UNITED
app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
#--------------------------------------------------------------------------------------
# styling the sidebar
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 64,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
    "overflow": "scroll",
}

# padding for the page content
CONTENT_STYLE = {
    "margin-left": "17rem",
    "margin-right": "1rem",
    "margin-top": "4rem",
    "padding": "1rem 0rem",
}

NAVBAR_STYLE = {
    "top": 0,
    "left": 0,
    "right": 0,
    "height": 50,
    "padding": "2rem 1rem",
}

sidebar = html.Div(
    [html.Div(
            [
                # width: 3rem ensures the logo is the exact width of the
                # collapsed sidebar (accounting for padding)
                html.Img(src='/assets/logo.png', style={"width": "3rem"}),
                html.H2("SHAYGAN"),
            ],
            className="sidebar-header",
        ),
        html.H2("Sidebar", className="display-5"),
        html.Hr(),
        html.P(
            "Number of students per education level", className="lead"
        ),
    ],
    style=SIDEBAR_STYLE,
)

# search_bar = dbc.Row(
#     [
#         dbc.Col(dbc.Input(type="search", placeholder="Search")),
#         dbc.Col(
#             dbc.Button(
#                 "Search", color="primary", className="ms-2", n_clicks=0
#             ),
#             width="auto",
#         ),
#     ],
#     className="g-0 ms-auto flex-nowrap mt-3 mt-md-0",
#     align="center",
# )

navbar = dbc.Navbar(
    dbc.Container(
        [html.Div(
                [
                    # width: 3rem ensures the logo is the exact width of the
                    # collapsed sidebar (accounting for padding)
                    html.Img(src='/assets/logo.png', style={"width": "3rem"}),
                    html.H2("SHAYGAN"),
                ],
                className="sidebar-header",
            ),
            html.Div(
                    [
                        # width: 3rem ensures the logo is the exact width of the
                        # collapsed sidebar (accounting for padding)
                        html.Img(src='/assets/Shaygan_Main_Logo_11.png', style={"width": "10rem"}),
                        html.H2("SHAYGAN"),
                    ],
                    className="sidebar-header",
                ),
            html.A(
                # Use row and col to control vertical alignment of logo / brand
                dbc.Row(
                    [
                        dbc.Col(dbc.NavbarBrand("Data Driven Solutions", className="lead ms-2 fs-3 text"), width=10),
                        dbc.Col(dbc.NavbarBrand("shaygan-tele", className="lead ms-2 fs-5 text"), width=2),
                    ],
                    align="center",
                    className="g-0",
                ),
                style={"textDecoration": "none", "font-style": "italic"},
            ),
            dbc.NavbarToggler(id="navbar-toggler", n_clicks=0),
            dbc.Collapse(
                # search_bar,
                # id="navbar-collapse",
                # is_open=False,
                # navbar=True,
            ),
        ]
    ),
    color="dark",
    dark=True,
    fixed="top",
    style=NAVBAR_STYLE,
)


# add callback for toggling the collapse on small screens
# @app.callback(
#     Output("navbar-collapse", "is_open"),
#     [Input("navbar-toggler", "n_clicks")],
#     [State("navbar-collapse", "is_open")],
# )
# def toggle_navbar_collapse(n, is_open):
#     if n:
#         return not is_open
#     return is_open

accordion = html.Div(
    dbc.Accordion(
        [
            dbc.AccordionItem(
                "This is the content of the first section",
                title="Item 1",
            ),
        ],
        always_open=False,
    )
)
#--------------------------------------------------------------------------------------
app.layout = dbc.Container([
    html.Div(
            [
                # width: 3rem ensures the logo is the exact width of the
                # collapsed sidebar (accounting for padding)
                html.Img(src='/assets/logo.png', style={"width": "3rem"}),
                html.H2("SHAYGAN"),
            ],
            className="sidebar-header",
        ),
    dbc.Row([
        dbc.Col(navbar, width=12),
        html.Div(
                [
                    # width: 3rem ensures the logo is the exact width of the
                    # collapsed sidebar (accounting for padding)
                    html.Img(src='/assets/logo.png', style={"width": "3rem"}),
                    html.H2("SHAYGAN"),
                ],
                className="sidebar-header",
            ),
    ]),

    dbc.Row([
        dbc.Col(sidebar, width=2),
    ]),

    html.Div([

        dbc.Row([
            dbc.Col(accordion, width=12),
        ]),

        dbc.Row([
            dbc.Col([
                html.Br(),
                html.H2("Network Radio Ticket Analytics"),  # style={"textAlign": "left"}),
                html.Br(),
            ], width=6)
        ], justify='left', ),

        dbc.Row([
            dbc.Col([
                html.Label('Province'),
                dcc.Dropdown([x for x in sorted(ticket_df["Index"].unique())], multi=False, id='pro_line_dpdn', ),
                html.Label('KPI'),
                dcc.Dropdown([x for x in sorted(ticket_df["DEGRADED_KPI"].unique())], multi=False, id='kpi_line_dpdn',
                             style={'font-size': 12}),
            ], width=2),
            dbc.Col([
                html.H3("Ticket Trend", style={"textAlign": "center"}, className='text-muted'),
                dcc.Graph(id='line_tt_trend', config={'displayModeBar': False}, className='bg-light')
            ], width=10),
        ], className='shadow p-2 bg-light rounded'),

    ], style=CONTENT_STYLE)

], fluid=True, className="ps-0 pe-0")
#--------------------------------------------------------------------------------------
#                                         callbacks
#--------------------------------------------------------------------------------------
# Line Chart for tickets' trend
@app.callback(
    Output(component_id='line_tt_trend', component_property='figure'),
    [Input(component_id='pro_line_dpdn', component_property='value'),
     Input(component_id='kpi_line_dpdn', component_property='value')])

def update_graph(chosen_province, chosen_kpi):

    line_df = ticket_df
    if chosen_province is not None:
        line_df = line_df[line_df["Index"] == chosen_province]
    if chosen_kpi is not None:
        line_df = line_df[line_df["DEGRADED_KPI"] == chosen_kpi]


    # preparing data for tickets' trend line chart
    line_df = line_df.groupby(['REPORTED'], as_index=False)['PROM_NUMBER'].count()
    line_df = line_df.set_index('REPORTED')
    line_df = line_df.groupby([pd.Grouper(freq="D")])['PROM_NUMBER'].sum().reset_index()

    # preparing tickets' trend line chart
    line_fig=px.line(line_df, x="REPORTED", y="PROM_NUMBER", height=450, markers=True)

    line_fig.update_traces(hovertemplate='%{y}', line_shape='spline')

    line_fig.update_layout(xaxis={'title': '', 'showgrid':False },
                           yaxis={'title': 'Number of Tickets', 'showgrid':False},
                           margin={'l':0,'t':30,'b':0,'r':0},
                           paper_bgcolor="#dee2e6",
                           plot_bgcolor="#dee2e6",
                           hovermode="x",
                           )
    return (line_fig)
#--------------------------------------------------------------------------------------

#--------------------------------------------------------------------------------------
if __name__ == "__main__":
    app.run_server(debug=False)
